{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "zebcache-helm.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "zebcache-helm.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "zebcache-helm.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "zebcache-helm.labels" -}}
helm.sh/chart: {{ include "zebcache-helm.chart" . }}
{{ include "zebcache-helm.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "zebcache-helm.selectorLabels" -}}
app.kubernetes.io/name: {{ include "zebcache-helm.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app: "zebcache"
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "zebcache-helm.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "zebcache-helm.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/* Generates a list of altNames */}}
{{- define "gen-zebcache-hostnames" -}}
{{- range $i := until (int .Values.global.zebcache.instances) }}{{ $.Release.Name }}-{{ $.Chart.Name }}-{{$i}}.{{ $.Release.Name }}-{{ $.Chart.Name }},{{ end }}
{{- end }}

{{/*
Renders a value that contains template.
Usage:
{{ include "zebcache-helm.tplValue" ( dict "value" .Values.path.to.the.Value "context" $) }}
*/}}
{{- define "zebcache-helm.tplValue" -}}
    {{- if typeIs "string" .value }}
        {{- tpl .value .context }}
    {{- else }}
        {{- tpl (.value | toYaml) .context }}
    {{- end }}
{{- end -}}