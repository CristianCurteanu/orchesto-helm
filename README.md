# Orchesto-helm

This repo will help you setup Orchesto on a kubernetes cluster by installing some pre-requisite software, configure your `values.yaml` and execute `helm`, optionally with some parameters.

### The following software will be deployed

**Prerequisites**

- ingress-nginx
- cert-manager

**Charts**

- orchesto
- zebcache
- orchesto-analysis

**Optional Charts**

- postgresql

## Prerequisites

The following software must be installed before proceeding with the installation.

### ingress-nginx

This helm chart uses a nginx ingress. To install the `ingress-nginx` helm repo run:

```sh
$ helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
```

### cert-manager

Orchesto uses cert-manager for managing certificates.

The latest installation instruction for cert-manager can be found [here](https://cert-manager.io/docs/installation/kubernetes/)

## Configuration

The following to sections describe how to configure our helm chart by modifying `values.yaml` and optionally by input parameters to `helm`.

The main parts of the configuration consists of

- license and product keys
- ingress
- certificates
- database connection
- resources

### License and product keys

Can be updates in `values.yaml` or added as parameters to helm with `--set license=<license key> --set product=<product key>`

### Ingress

The ingress can only be configured in `values.yaml`.

The default configuration is set to use `lets_encrypt: false` and `localhost` as hostname. This will produce a selfsigned-certificate for localhost.

values.yaml
```yaml
lets_encrypt: false

ingress:
  enabled: true
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
  hosts:
    - host: localhost # change to your domain
      paths:
        path: /
  tls:
    - hosts:
      - localhost # change to your domain
```

For production use you might want to use real certificates for your domain using let's encrypt

values.yaml

``` yaml
lets_encrypt: true

ingress:
  enabled: true
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
  hosts:
    - host: example.com
      paths:
        path: /
  tls:
    - hosts:
      - example.com
```

This would use cert-manger to configure a lets encrypt issuer and request a production certificate for `example.com`.

### Certificates

By default you will get a self-signed certificate from cert-manager. To use Let's Encrypt your and use the parameter `lets_encrypt` and set it to true either by updating your `values.yaml` or add `--set lets_encrypt=true` as a parameter when you run helm.

### Database connection

The database connection is configure with `db.dsn` either in `values.yaml` or as a parameter to helm with `--set db.dsn="my-connection-string"`.

### Resources

`replicaCount`: configures the number of orchesto pods

`global.zebcache.instances`: configures the number of zebcache pods

**Parameters**

Parameters can be configured directly in `values.yaml` or be given as input when running `helm`.

| Parameter | Description | Type | Default | 
|---        | ---         | ---  | ---     |
| token | token that will be used to initialize your instance, must be set | String | "token" |
| license | your orchesto license | String | nil |
| product | your orchesto product id | String | nil |
| db.dsn | database connection string. Defaults to shipped instance | String | `postgres://orchesto:orchesto@orchesto-postgresql:5432/orchesto?sslmode=disable` |
| postgresql.initdbScripts.orchesto.sql | contains postgres role username and password | String |CREATE ROLE orchesto WITH LOGIN PASSWORD 'orchesto'; CREATE DATABASE orchesto with OWNER = orchesto LC_CTYPE 'C' LC_COLLATE 'C' ENCODING=UTF8 TEMPLATE=template0; |
| db.backup.enabled | enables backup to s3 compatible object store | bool | `false` |
| lets_encrypt | add letsencrypt issuer and requests a production certificate for your domain | bool | `false` |
| replicaCount | Number of orchesto pods | int | `3` |
| global.zebcache.instances | Number of zebcache pods, requires a minimum of 3 nodes | int | `3` |
| resources | CPU/Memory resource requests/limits | map | {} |
| serviceAccount.name | name of service account | String | "orchesto-sa" |
| nameOverride | String to partially override orchesto.fullname template with a string (will prepend the release name) | String | "orchesto" |
| fullnameOverride | String to fully override postgresql.fullname template with a string | String | nil |
| imagePullSecrets | Docker registry secret names as an array | String | [] |

A more extensive list for all the parameters to the subchart postgresql can be found [here](https://github.com/bitnami/charts/tree/master/bitnami/postgresql#parameters)

**Optional parameters**

| Parameter | Description | Type | Default | 
|---        | ---         | ---  | ---     |
|postgresSecretsPassphrase| admin password in postgresql chart | String | nil |

## Example deployments

Before deploying we should retrieve all dependencies

```sh
$ helm dependency update
```

Now we can install the chart. Make sure you updated your `values.yaml` or add your input parameters now


### localhost and self signed certificates

This chart will require `license` and `product` to be set at a minimum. With no changes in `values.yaml` the following will install Orchesto to your cluster using a self-signed certificate and the hostname would be configured to `localhost`:

```sh
$ helm install orchesto . --set license=<license> --set product=<product key> 
```

### Use external postgres database

In case you are running your own postgres and want to disable the postgres that we ship:

```sh
$ helm install orchesto .
  --set license=<license> \
  --set product=<product key> \
  --set postgresql.enabled=false \
  --set db.dsn="my-postgres.example.com"
```

### Use let's encrypt

Using a production certificate from let's encrypt

First update the ingress configuration in `values.yaml`

```yaml
ingress:
  enabled: true
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
    nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
  hosts:
    - host: mydomain.com # change to your domain
      paths:
        path: /
  tls:
    - hosts:
      - mydomain.com # change to your domain
```

And run helm with `lets_encrypt=true`

```sh
$ helm install orchesto . \
  --set license=<license> \
  --set product=<product key> \
  --set lets_encrypt=true
```

## Access Orchesto
----

Point your domain to the external ip of Orchesto.

Once you can access Orchesto through your domian https://<domain> you can go ahead and initialize orchesto.


```sh
$ curl -k -X PUT \
  -H "Content-Type: application/json" \
  -d '{"token":"token"}' https://<domain>/orchesto/api/v1/admin/init
```

Make sure to store the output. You will not be able to retrieve it again.

Login with `admin` and your `secretAccessKey`


## S3 Backups

You can configure your `values.yaml` file to enable a cronjob to take backups of your database to a s3 compatible object store

values.yaml
```yaml
db:
  s3_backup:
    enabled: true
    schedule: "0 2 * * *" # cron schedule
    config:
      s3_endpoint: <s3 compatible endpoint>
      s3_access_key: <access key>
      s3_secret_key: <secret access key>
      s3_bucket: /orchesto-backups
```

This will schedule a pod to run every night at 2 am and create a database backup which is stored on your `s3_endpoint`.

## Default Pod Layout

Example of the pods deployed by a default installation

| Chart | Number of pods  |
|---                | --- |
| orchesto-helm     | 3   |
| orchesto-analysis | 1   |
| zebcache-helm     | 3   |
| postgresql        | 1   |
| cert-manager      | 3   |

| Pod name | Chart |
|---        | --- |
| orchesto-ingress-nginx-controller-5d4d7655b8-4mvml | ingress-nginx     |
| orchesto-orchesto-helm-7784cf864c-bjg4d            | orchesto-helm     |
| orchesto-orchesto-helm-7784cf864c-ds6j4            | orchesto-helm     |
| orchesto-orchesto-helm-7784cf864c-jhnjg            | orchesto-helm     |
| orchesto-analysis-6fb9dc97fc-rcr4x                 | orchesto-analysis |
| orchesto-postgresql-0                              | postgresql        |
| orchesto-zebcache-helm-0                           | zebcache-helm     |
| orchesto-zebcache-helm-1                           | zebcache-helm     |
| orchesto-zebcache-helm-2                           | zebcache-helm     |

Cert-manager will run in it's own namespace

| Pod name | Chart |
|---        | --- |
| cert-manager-649c5f88bc-7tklk         | cert-manager |
| cert-manager-cainjector-9747d56-57kph | cert-manager |
| cert-manager-webhook-849c7b574f-fjtkz | cert-manager |

## Uninstall

To uninstall orchesto from your cluster simply run `$ helm uninstall orchesto`.

You will still have to remove your `pvc`'s and `secret`'s since these are not removed by helm.

Get secrets
```sh
$ kubectl get secrets
```

Delete secrets
```sh
$ kubectl delete secrets <name> <name> <name> ...
```

Get pvc's
```sh
$ kubectl get pvc
```

Delete pvc's
```sh
$ kubectl delete pvc <name> <name> <name> ...
```
