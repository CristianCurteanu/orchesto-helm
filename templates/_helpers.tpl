{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "orchesto-helm.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "orchesto-helm.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "orchesto-helm.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "orchesto-helm.labels" -}}
helm.sh/chart: {{ include "orchesto-helm.chart" . }}
{{ include "orchesto-helm.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "orchesto-helm.selectorLabels" -}}
app.kubernetes.io/name: {{ include "orchesto-helm.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app: "orchesto"
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "orchesto-helm.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "orchesto-helm.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/* Generates a string of zebcache nodes */}}
{{- define "zebcache-cache-remotes" -}}
{{- range $i := until (int .Values.global.zebcache.instances) }}orchesto-zebcache-helm-{{$i}}.orchesto-zebcache-helm:17110; {{ end }}
{{- end }}

{{/* Generates a list of zebcache nodes */}}
{{- define "zebcache-hostname-list" -}}
{{- range $i := until (int .Values.global.zebcache.instances) }}
- orchesto-zebcache-helm-{{$i}}.orchesto-zebcache-helm{{ end }}
{{- end }}

{{/* generate correct ca-issuer name */}}
{{- define "ca-issuer" }}
{{ if eq .Values.lets_encrypt true }}
cert-manager.io/issuer: "ca-issuer-letsencrypt"
{{ else }}
cert-manager.io/issuer: "selfsigned-issuer"
{{ end }}
{{- end }}

{{/* generate correct cert-secret name */}}
{{- define "cert-secret" }}
{{ if eq .Values.lets_encrypt true }}
secretName: "letsencrypt-cert-secret"
{{ else }}
secretName: "selfsigned-cert-secret"
{{ end }}
{{- end }}

{{/* Get domain name from ingress */}}
{{- define "dns-names" -}}
{{- range $i := .Values.ingress.hosts }}
- {{ $i.host | quote }}{{ end }}
{{- end }}

{{/* Create orchesto.conf secret */}}
{{- define "orchesto-conf" -}}
dsn: {{ .Values.db.dsn }}
secrets.postgres.masterPassphrase: {{ .Values.db.postgresSecretsPassphrase }}
cache.remotes: {{ ( include "zebcache-cache-remotes" . ) | trimSuffix "; " }}
cache.dataShards: 2
cache.parityShards: 1
cache.performanceProfile: balanced
cache.obfuscateShards: false
cache.caFile: /zebcache/certs/server/ca.crt
cache.certFile: /zebcache/certs/server/tls.crt
cache.keyFile: /zebcache/certs/server/tls.key
https.certFile: /root/certs/tls.crt
https.keyFile: /root/certs/tls.key
tikaUrl: http://orchesto-analysis:9998

{{- end }}

{{/* Create license.json secret */}}
{{- define "orchesto-license" -}}
{"orchestoLicense": "{{ .Values.license }}","product": "{{ .Values.product }}"}
{{- end }}
