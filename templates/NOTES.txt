 _______             ______             _____
 __  __ \_______________  /_______________  /______ 
 _  / / /_  ___/  ___/_  __ \  _ \_  ___/  __/  __ \
 / /_/ /_  /   / /__ _  / / /  __/(__  )/ /_ / /_/ /
 \____/ /_/    \___/ /_/ /_/\___//____/ \__/ \____/         https://zebware.com


1. Get the application URL by running these commands:

{{- if contains "NodePort" .Values.service.type }}
   $ export NODE_PORT=$(kubectl get --namespace {{ .Release.Namespace }} -o jsonpath="{.spec.ports[0].nodePort}" services {{ include "orchesto-helm.fullname" . }})
   $ export NODE_IP=$(kubectl get nodes --namespace {{ .Release.Namespace }} -o jsonpath="{.items[0].status.addresses[0].address}")

2. Initialize orchesto:
   NOTE: The output must be stored safetly. You will not be able to retrieve the access and secret access key again!
   
   $ curl -k -X PUT -H "Content-Type: application/json" -d '{"token":"{{ .Values.token }}"}' https://$NODE_IP:$NODE_PORT/orchesto/api/v1/admin/init
  

3. Visit https://$SERVICE_IP:{{ .Values.service.port }}


{{- else if contains "LoadBalancer" .Values.service.type }}
   NOTE: It may take a few minutes for the LoadBalancer IP to be available.
         You can watch the status of by running: 
         $ 'kubectl get --namespace {{ .Release.Namespace }} svc -w {{ include "orchesto-helm.fullname" . }}'
  
   $ export SERVICE_IP=$(kubectl get svc --namespace {{ .Release.Namespace }} {{ include "orchesto-helm.fullname" . }} --template "{{"{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}"}}")
  
2. Initialize orchesto:
   NOTE: The output must be stored safetly. You will not be able to retrieve the access and secret access key again!
   
   $ curl -k -X PUT -H "Content-Type: application/json" -d '{"token":"{{ .Values.token }}"}' https://$SERVICE_IP:{{ .Values.service.port }}/orchesto/api/v1/admin/init
  
3. Visit https://$SERVICE_IP:{{ .Values.service.port }}

  
{{- else if contains "ClusterIP" .Values.service.type }}
   $ export POD_NAME=$(kubectl get pods --namespace {{ .Release.Namespace }} -l "app.kubernetes.io/name={{ include "orchesto-helm.name" . }},app.kubernetes.io/instance={{ .Release.Name }}" -o jsonpath="{.items[0].metadata.name}")
   $ kubectl --namespace {{ .Release.Namespace }} port-forward $POD_NAME 9090:9090
   $ export SERVICE_IP=$(kubectl get svc --namespace {{ .Release.Namespace }} {{ include "orchesto-helm.fullname" . }} --template "{{"{{ range (index .status.loadBalancer.ingress 0) }}{{.}}{{ end }}"}}")
  
2. Initialize orchesto:
   NOTE: The output must be stored safetly. You will not be able to retrieve the access and secret access key again!
   $ curl -k -X PUT -H "Content-Type: application/json" -d '{"token":"{{ .Values.token }}"}' https://localhost/orchesto/api/v1/admin/init
  
3. Visit https://$SERVICE_IP:{{ .Values.service.port }}
  
  
{{- end }}
